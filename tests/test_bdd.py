import logging

# pour afficher les traces dans la console
import sys
import unittest
from datetime import datetime

import bdd.connection

root = logging.getLogger()
root.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
# formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(pathname)s - ligne:%(lineno)s - %(message)s')
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

print('===================== TestBDD ==============================')


class TestBDD(unittest.TestCase):
    def setUp(self) -> None:
        pass
    @unittest.skip('skip')
    def test_table(self):
        print('=== test_table ===')

        table_name = 'table_essai'

        # efface les tables
        resultat = bdd.table.delete('table_essai2')
        self.assertIsNone(resultat)
        resultat = bdd.table.delete(table_name)
        self.assertIsNone(resultat)

        # --- cas creation de la table ---
        colonne = [{'col': 'Colonne1', 'type': 'text', 'constraint': ''}, {'col': 'Colonne2', 'type': 'text', 'constraint': ''}]
        resultat = bdd.table.create(table_name, colonne)
        self.assertIsNone(resultat)

        colonne = [
            {
                'col': 'id_table_essai',
                'type': 'int',
                'constraint': [
                    {'nom': 'FK_table_essai2_id_table_essai', 'sql': 'FOREIGN KEY(id_table_essai) REFERENCES table_essai(id)'}
                ],
            },
            {'col': 'Colonne1', 'type': 'text', 'constraint': ''},
            {'col': 'Colonne2', 'type': 'text', 'constraint': ''},
        ]
        resultat = bdd.table.create('table_essai2', colonne)
        self.assertIsNone(resultat)

        # --- cas insertion données ok ---
        data = {'colonne1': 'contenu 1', 'colonne2': 'contenu 2'}
        id_essai = bdd.table.insert(table_name, data, 'id')
        self.assertIsInstance(id_essai, int)

        # --- cas insertion données ko colonne absente ---
        data = {'colone1': 'contenu 1', 'colonne2': 'contenu 2'}
        resultat = bdd.table.insert(table_name, data)
        # self.assertEqual(resultat, "ERREUR:  la colonne « colone1 » de la relation « table_essai » n'existe pas")
        self.assertIn(
            resultat,
            {
                'ERROR:  column "colone1" of relation "table_essai" does not exist',
                "ERREUR:  la colonne « colone1 » de la relation « table_essai » n'existe pas",
            },
        )

        # --- cas insertion données ko table absente ---
        data = {'colonne1': 'contenu 1', 'colonne2': 'contenu 2'}
        resultat = bdd.table.insert('table_erreur', data)
        self.assertIn(
            resultat, {'ERROR:  relation "table_erreur" does not exist', "ERREUR:  la relation « table_erreur » n'existe pas"}
        )

        # lecture données
        sql = """select *
                 from {table_name}
                 where id = %s""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql, (id_essai,))
        resultat[0].pop('id')
        self.assertDictEqual(resultat[0], data)

        # --- insertion données injection ---
        data = {'colonne1': 'contenu 1', 'colonne2': ');drop table_name;'}
        id_essai = bdd.table.insert(table_name, data)
        self.assertIsInstance(id_essai, int)

        # lecture données
        sql = """select *
                 from {table_name}
                 where id = %s""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql, (id_essai,))
        resultat[0].pop('id')
        self.assertDictEqual(resultat[0], data)

        # --- cas lecture données multiple ---
        sql = """select *
                 from {table_name}
                 where colonne1 = %s""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql, ('contenu 1',))

        # ajout de colonnes
        colonne = [
            {'col': 'Colonne3', 'type': 'int', 'constraint': ''},
            {'col': 'Colonne4', 'type': 'timestamp', 'constraint': ''},
        ]
        bdd.table.create(table_name, colonne)

        # --- cas insertion données datetime et nombre ---
        now = datetime.now()
        data = {'colonne1': 'contenu 1', 'colonne2': 'contenu 2', 'colonne3': 1, 'colonne4': now}
        id_essai = bdd.table.insert(table_name, data)
        self.assertIsInstance(id_essai, int)

        # lecture données
        sql = """select *
                 from {table_name}
                 where id = %s""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql, (id_essai,))
        resultat[0].pop('id')
        self.assertDictEqual(resultat[0], data)

        # --- cas update ---
        now = datetime.now()
        id_essai = 1
        data = {'colonne1': 'modifie contenu 1', 'colonne2': 'modifie contenu 2', 'colonne3': id_essai, 'colonne4': now}
        condition = {'id': 1}
        resultat = bdd.table.update(table_name, condition, data)
        self.assertIsNone(resultat)

        # lecture données
        sql = """select *
                 from {table_name}
                 where id = %s""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql, (id_essai,))
        resultat[0].pop('id')
        self.assertDictEqual(resultat[0], data)

        # lecture de toutes les données
        sql = """select *
                 from {table_name}""".format(
            table_name=table_name
        )
        resultat = bdd.table.select(sql)
        self.assertEqual(len(resultat), 3)

        # --- cas insertion données datetime et nombre via upsert ---
        now = datetime.now()
        data = {'colonne1': 'contenu 1', 'colonne2': 'contenu 2', 'colonne3': 1, 'colonne4': now, 'id': 5}
        id_essai = bdd.table.upsert(table_name, data)
        self.assertIsInstance(id_essai, int)

        # --- cas mise à jour données via upsert ---
        now = datetime.now()
        data = {'colonne1': 'contenu 1 bis', 'colonne4': now, 'id': 5}
        id_essai = bdd.table.upsert(table_name, data)
        self.assertIsInstance(id_essai, int)
        sql = f"""select * from {table_name} where id = %s"""
        resultat = bdd.table.select(sql, (id_essai,))
        self.assertEqual(resultat[0]['colonne1'], 'contenu 1 bis')

        print('terminé')
