# Module de gestion de base de données postgresql 13

!!! Interdition d'utiliser "{}".format(val) pour les variables. Utiliser "%s" % val pour eviter tout risque d'injection sql !!!

## Effacement d'une table
delete(table_name):
- paramatres:
  - table_name: nom de la table à effacer
- retourne: (dict): résultat sous forme de dictionnaire {"clé(non de la colonne)": valeur,...}

## Création d'une table et l'ajout de colonnes
bdd.table.create(table_name, colonne)
- Parametres :
  - table_name (str): nom de la table
  - id_name (str): nom de la cle primaire
  - colonnes (list de dict):
    - "col": colonnes de la table
    - "type": leurs type
    - "constraint": contraintes
- Retourne:
  - en cas de reussite : None
  - en cas d’échec : le message d'erreur
- Exemple:
```code
  colonne = [
  {"col": "colonne1",
  "type": "text",
  "constraint": ''},
  {"col": "Colonne2",
  "type": "text",
  "constraint": ''}
  ]
  resultat = bdd.table.create(table_name, colonne)
```

## Lecture de lignes d'une table
def select(sql, data=None):
- Arguments:
  - sql (str): requête sql
  - data (tuple): données (optionnel si pas de clause where)
- Retourne: (dict):
   - en cas de réussite :
     résultat sous forme de dictionnaire {"clé(non de la colonne)": valeur,...}
      - le type de données est :
        - datetime pour les timestamp
        - str pour les chaînes de caractères
        - int pour les entier
        - ...
  - en cas d’échec : le message d'erreur
- Exemple:
```code
sql = """select *
         from table_name
         where id_truc = %s and date > %s"""
data = (variable_id, variable_date)
resultat = bdd.table.select(sql,data)
```
```code
sql = """select * from table_name"""
resultat = bdd.table.select(sql)
```

## Insertion d'une ligne dans une table
insert(table_name, datas):
- Arguments:
  - table_name (str): nom de la table
  - datas (dict):  {"col": "valeur", ...}
    - exemple: {"colonne1": "contenu 1", "colonne2": "contenu 2"}
- Retourne:
  - en cas de réussite : l'id de l’élément inséré
  - en cas d’échec : le message d'erreur

# Mise à jour d'éléments d'une ligne dans une table
update(table_name, conditions, datas):
- Arguments:
  - table_name (str): nom de la table
  - condition (dict): clés de la ligne à modifier et sa valeur
  - datas (dict):  {"col": "valeur", ...}
    - exemple: {"colonne1": "contenu 1", "colonne2": "contenu 2"}
- Retourne:
  - en cas de réussite : None
  - en cas d’échec : le message d'erreur
- Exemple:
 ```code
  now = datetime.now()
  id_essai = 1
  data = {"colonne1": "modifie contenu 1",
          "colonne2": "modifie contenu 2",
          "colonne3": id_essai,
          "colonne4": now}
  condition = {"id": 1}
  resultat = bdd.table.update(table_name, condition, data)
```
