# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# -----------------------------------------------------------
# ---             centralisation des logs.                ---
# -----------------------------------------------------------
# -----------------------------------------------------------

import logging


def info(message):
    logging.info(message)


def debug(message):
    logging.debug(message)


def error(message):
    logging.error(message)





