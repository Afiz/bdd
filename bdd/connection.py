# -*- coding: utf-8 -*-
import psycopg2
import os

conn = None
# Set default value for database configuration or get environnment variables if available
host_value = 'localhost'
if "FDR_BDD_HOST" in os.environ:
    host_value = os.environ['FDR_BDD_HOST']

port_value = '5432'
if "FDR_BDD_PORT" in os.environ:
    port_value = os.environ['FDR_BDD_PORT']

dbname_value = 'fdr'
if "FDR_BDD_DBNAME" in os.environ:
    dbname_value = os.environ['FDR_BDD_DBNAME']

user_value = 'fdr'
if "FDR_BDD_USER" in os.environ:
    user_value = os.environ['FDR_BDD_USER']

pwd_value = 'fdr'
if "FDR_BDD_PASSWORD" in os.environ:
    pwd_value = os.environ['FDR_BDD_PASSWORD']


def get_conn():
    """
        Connection à la base de donnée
    """
    global conn
    if conn is None:
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
        
        conn = psycopg2.connect("host='{0}' port='{1}' dbname='{2}' user='{3}' password='{4}'".format(host_value,
                                                                                                      port_value,
                                                                                                      dbname_value,
                                                                                                      user_value,
                                                                                                      pwd_value))
    return conn


def close():
    global conn
    if conn is not None:
        conn.close()
    conn = None
