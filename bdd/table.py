# -*- coding: utf-8 -*-
"""
 Module de gestion de base de données postgresql 13
 Interdition d'utiliser "{}".format(val) pour les variables. Utiliser "%s" % val pour eviter tout risque d'injection sql
"""
import bdd
import bdd.log
import psycopg2.extras

conn = None


class FdrBddException(Exception):
    erreur = ""
    def __init__(self, message):
        self.erreur = message
        return


def delete(table_name):
    """
    Effacement d'une table
    :param table_name: nom de la table à effacer
    :return:
    """
    sql = """DROP TABLE if exists {table_name}""".format(table_name=table_name)
    bdd.log.debug(sql)

    conn = bdd.connection.get_conn()
    cur = conn.cursor()
    try:
        cur.execute(sql)
        conn.commit()
        return None
    except Exception as e:
        conn.commit()
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        raise FdrBddException(erreur)


def create(table_name, colonnes: dict, id_name="id"):
    """
    Permet la création d'une table et l'ajout de colonnes
     Args:
      table_name (str): nom de la table
      id_name (str): nom de la cle primaire
      colonnes (list de dict): ["col": colonnes de la table
                                "type": leurs type
                                "constraint": [{"nom":"FK_table_essai2_id_table_essai", "sql":"FOREIGN KEY(id_table_essai) REFERENCES table_essai(id)"}]]

    Le squelette d’ajout de contraintes est le suivant :
    => contraintes de valuation obligatoire TODO : A Faire
        ALTER TABLE maTable
        ALTER COLUMN maColonne memeType NOT NULL
    => contraintes de clé primaire
    nom = PK_maTable
    sql = PRIMARY KEY (colonnesConstituantLaCléPrimaire)
    => contraintes de clé étrangère
    nom = FK_maTable_colonneDeCleEtrangere
    sql = FOREIGN KEY(colonneDeCleEtrangere)
          REFERENCES tableContenantLaClePrimaireAReferencer(colonneDeClePrimaire)
    => contraintes d’unicité
    nom= UQ_maTable_colonnesConstituantLeTupleQuiDoitEtreUnique
    sql = UNIQUE (colonnesConstituantLeTupleQuiDoitEtreUnique)
    => contraintes de valeur par défaut :
    nom = DF_maTable_maColonne
    sql = DEFAULT (uneValeur) FOR (maColonne)

    Return: none
    """
    conn = bdd.connection.get_conn()
    cur = conn.cursor()

    # creation table
    sql = """CREATE TABLE IF NOT EXISTS "{table_name}" ("{id_name}" SERIAL primary key)
          """.format(table_name=table_name, id_name=id_name)
    bdd.log.debug(sql)
    try:
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        conn.commit()
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        raise FdrBddException(erreur)

    # creation des colonnes
    for index in colonnes:
        sql = """ALTER TABLE "{table_name}" ADD COLUMN IF NOT EXISTS {column_name} {column_type}""".format(
            table_name=table_name,
            column_name=index['col'],
            column_type=index['type'])
        bdd.log.debug(sql)
        try:
            cur.execute(sql)
            conn.commit()
        except Exception as erreur:
            conn.commit()
            bdd.log.error(erreur)
            raise FdrBddException(erreur)

        # creation des contrainte
        if 'constraint' in index:
            for contrainte in index['constraint']:
                sql = """ALTER TABLE "{table_name}" DROP CONSTRAINT IF EXISTS {contrainte_nom};
                         ALTER TABLE "{table_name}" ADD CONSTRAINT {contrainte_nom} {contrainte_sql}
                                      """.format(table_name=table_name, column_name=index['col'],
                                                 contrainte_nom=contrainte["nom"],
                                                 contrainte_sql=contrainte["sql"])
                bdd.log.debug(sql)
                try:
                    cur.execute(sql)
                    conn.commit()
                except Exception as erreur:
                    conn.commit()
                    bdd.log.error(erreur)
                    raise FdrBddException(erreur)
    return None


def select(sql, data=None):
    """
      Permet la lecture de lignes d'une table
       Args:
        sql (str): requête sql
        data (tuple): données (optionnel si pas de clause where)
       Return (dict): - en cas de réussite :
                        résultat sous forme de dictionnaire {"clé(non de la colonne)": valeur,...}
                            le type de données est :
                                datetime pour les timestamp
                                str pour les chaînes de caractères
                                int pour les entier
                                ...
                     - en cas d’échec : le message d'erreur
    """
    if data is not None:
        bdd.log.debug(sql % data)
    else:
        bdd.log.debug(sql)

    conn = bdd.connection.get_conn()
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    try:
        if data is not None:
            cur.execute(sql, data)
        else:
            cur.execute(sql)
        resultat = cur.fetchall()
        if len(resultat) > 0:
            resultat = [dict(row) for row in resultat]
        return resultat
    except Exception as e:
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        raise FdrBddException(erreur)


def insert(table_name, datas, id_key='id'):
    """
      Permet l'insertion d'une ligne dans une table
       Args:
        table_name (str): nom de la table
        datas (dict):  {"col": "valeur", ...}
               exemple: {"colonne1": "contenu 1",
                         "colonne2": "contenu 2"}
        id_key (str): clé primaire de la ligne à retourner ('id' par défaut si non précisé)
       Return: - en cas de réussite : l'id de l’élément inséré
               - en cas d’échec : le message d'erreur
    """
    values = []
    sql_col = ""
    sql_values = ""
    nb = 0
    for col, valeur in datas.items():
        nb = nb + 1
        sql_col += col
        sql_values += "%s"
        values.append(valeur)
        if nb < len(datas):
            sql_col += ", "
            sql_values += ", "
    # formatage de la colonne retourné
    #values.append(id_key)

    sql = """INSERT INTO {table_name} ({sql_col}) VALUES ({sql_values}) RETURNING *""".format(table_name=table_name, sql_col=sql_col, sql_values=sql_values)
    sql_debug = sql % tuple(values)
    bdd.log.debug(sql_debug)

    conn = bdd.connection.get_conn()
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cur.execute(sql, values)
        resultat = cur.fetchone()
        conn.commit()
        for row, val in resultat.items():
            if row == id_key:
                return val
        erreur = "colonne %s non trouvée" % id_key
        bdd.log.error(erreur)
        raise FdrBddException(erreur)
    except Exception as e:
        conn.commit()
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        raise FdrBddException(erreur)


def update(table_name, conditions, datas):
    """
      Permet la mise à jour d'éléments d'une ligne dans une table
       Args:
        table_name (str): nom de la table
        condition (dict): clés de la ligne à modifier et sa valeur TODO: ajoute gestion des ou et des et
        datas (dict):  {"col": "valeur", ...}
               exemple: {"colonne1": "contenu 1",
                         "colonne2": "contenu 2"}
       Return: - en cas de réussite : None
               - en cas d’échec : le message d'erreur
    """

    # formatage des données à modifier
    values = []
    sql_data = ""
    nb = 0
    for col, valeur in datas.items():
        nb = nb + 1
        sql_data += col + " = %s"
        values.append(valeur)
        if nb < len(datas):
            sql_data += ", "

    # formation de la condition (le where sql)
    sql_condition = ""
    nb = 0
    for col, valeur in conditions.items():
        nb = nb + 1
        sql_condition += col + " = %s"
        values.append(valeur)
        if nb < len(conditions):
            sql_condition += "and " #TODO ajouté or

    sql = """Update {table_name} SET {sql_data} where {sql_condition}""".format(table_name=table_name,
                                                                                sql_data=sql_data,
                                                                                sql_condition=sql_condition)
    sql_debug = sql % tuple(values)
    bdd.log.debug(sql_debug)

    conn = bdd.connection.get_conn()
    cur = conn.cursor()
    try:
        cur.execute(sql, values)
        conn.commit()
        return None
    except Exception as e:
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        conn.commit()
        raise FdrBddException(erreur)


def upsert(table_name, datas, id_key='id'):
    """
      Permet l'insertion d'une ligne dans une table ou la mise à jour si l'element existe deja
       Args:
        table_name (str): nom de la table
        datas (dict):  {"col": "valeur", ...}
               exemple: {"colonne1": "contenu 1",
                         "colonne2": "contenu 2"}
        id_key (str): clé primaire de la ligne à retourner ('id' par défaut si non précisé)
       Return: - en cas de réussite : l'id de l’élément inséré
               - en cas d’échec : le message d'erreur
    """
    values = []
    sql_col = ""
    sql_values = ""
    nb = 0
    for col, valeur in datas.items():
        nb = nb + 1
        sql_col += col
        sql_values += "%s"
        values.append(valeur)
        if nb < len(datas):
            sql_col += ", "
            sql_values += ", "
    # formatage de la colonne retourné
    #values.append(id_key)

    # formatage des données à modifier
    values_modif = []
    sql_data = ""
    nb = 0
    for col, valeur in datas.items():
        if col != id_key:
            nb = nb + 1
            sql_data += col + " = %s"
            values_modif.append(valeur)
            if nb < len(datas)-1:
                sql_data += ", "

    sql_condition = table_name+'.'+id_key + ' = ' + str(datas[id_key])

    sql = """INSERT INTO {table_name} ({sql_col}) VALUES ({sql_values}) 
    ON CONFLICT({id_key}) DO 
    UPDATE SET {sql_data} WHERE {sql_condition} 
    RETURNING *""".format(table_name=table_name, sql_col=sql_col, sql_values=sql_values, sql_data=sql_data,
                          sql_condition=sql_condition, id_key=id_key)
    sql_debug = sql % tuple(values+values_modif)
    bdd.log.debug(sql_debug)

    conn = bdd.connection.get_conn()
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cur.execute(sql, values+values_modif)
        resultat = cur.fetchone()
        conn.commit()
        for row, val in resultat.items():
            if row == id_key:
                return val
        erreur = "colonne %s non trouvée" % id_key
        bdd.log.error(erreur)
        raise FdrBddException(erreur)
    except Exception as e:
        conn.commit()
        erreur = e.pgerror.split('\n')[0]
        bdd.log.error(erreur)
        raise FdrBddException(erreur)
